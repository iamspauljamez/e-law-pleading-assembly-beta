class PleadingTemplatesController < ApplicationController
  before_action :set_pleading_template, only: [:show, :edit, :update, :destroy]

  # GET /pleading_templates
  # GET /pleading_templates.json
  def index
    @pleading_templates = PleadingTemplate.all
  end

  # GET /pleading_templates/1
  # GET /pleading_templates/1.json
  def show
    @pleading_templates = PleadingTemplate.all
    params[:complete] = "#{@pleading_template.case_heading}
                          #{@pleading_template.case_title}
                          #{@pleading_template.case_no}
                          #{@pleading_template.nature_of_case}
                          #{@pleading_template.pleadings_intro}
                          #{@pleading_template.copy_furnished}";
                 
    @complete_content = params[:complete]


    respond_to do |format|
      format.html

      format.docx do
        set_header('docx', "#{@pleading_template.id}.docx")   
        @content_for_docx = params[:complete]
      end
    end

  end

  # GET /pleading_templates/new
  def new
    @pleading_template = PleadingTemplate.new
  end

  # GET /pleading_templates/1/edit
  def edit
  end

  # POST /pleading_templates
  # POST /pleading_templates.json
  def create
    @pleading_template = PleadingTemplate.new(pleading_template_params)

    respond_to do |format|
      if @pleading_template.save
        format.html { redirect_to @pleading_template, notice: 'Pleading template was successfully created.' }
        format.json { render action: 'show', status: :created, location: @pleading_template }
      else
        format.html { render action: 'new' }
        format.json { render json: @pleading_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pleading_templates/1
  # PATCH/PUT /pleading_templates/1.json
  def update
    respond_to do |format|
      if @pleading_template.update(pleading_template_params)
        format.html { redirect_to @pleading_template, notice: 'Pleading template was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pleading_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pleading_templates/1
  # DELETE /pleading_templates/1.json
  def destroy
    @pleading_template.destroy
    respond_to do |format|
      format.html { redirect_to pleading_templates_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pleading_template
      @pleading_template = PleadingTemplate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pleading_template_params
      params.require(:pleading_template).permit(:case_heading, :case_title, :case_no, :nature_of_case, :pleadings_intro, :copy_furnished)
    end
end
