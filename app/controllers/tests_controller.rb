class TestsController < ApplicationController

  # def show
  #   params[:docx_html_source] = '<html><head></head><body><p>#{@page.content}</p></body></html>';

  #   respond_to do |format|
  #     format.docx do
  #       file = Htmltoword::Document.create params[:docx_html_source], "file_name.docx"
  #       send_file file.path, :disposition => "attachment"
  #     end
  #   end
  # end

  def show
    # @item = Item.find params[:id]
    # @item = '<html><head></head><body><p>#{@page.content}</p></body></html>';
    @page = Page.find params[:id]

    respond_to do |format|
      format.html # show.html.erb
      # format.xml { render :xml => @item }
      # format.msword { set_header('msword', "#{@}.doc") }
      format.doc do
        set_header('doc', "test.doc")   
        @content = @page.content 
      end

      format.pdf do
          render :pdf => 'Coming soon...', :layout => false
      end
    end
   end

end
