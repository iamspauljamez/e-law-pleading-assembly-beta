module ApplicationHelper
  
  def flash_class(level)
    case level
      when :notice then "alert alert-warning"   
      when :success then "alert alert-success"
      when :error then "alert alert-error"
      when :alert then "alert alert-error"  
    end
  end

  def hidden_div_if(condition, attributes = {}, &block)
    if condition
      attributes["style"] = "display: none"
    end
      content_tag("li", attributes, &block)
    end
end
