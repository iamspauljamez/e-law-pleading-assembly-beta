json.array!(@pleading_templates) do |pleading_template|
  json.extract! pleading_template, :id, : case_heading, :case_title, :case_no, :nature_of_case, :pleadings_intro, :copy_furnished
  json.url pleading_template_url(pleading_template, format: :json)
end
