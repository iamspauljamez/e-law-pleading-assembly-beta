class PleadingTemplate < ActiveRecord::Base
  validates :case_heading, :case_title, :case_no, :nature_of_case, :pleadings_intro, :copy_furnished, presence: true
end
