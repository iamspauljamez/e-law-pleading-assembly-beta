require 'test_helper'

class PleadingTemplatesControllerTest < ActionController::TestCase
  setup do
    @pleading_template = pleading_templates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pleading_templates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pleading_template" do
    assert_difference('PleadingTemplate.count') do
      post :create, pleading_template: {  case_heading: @pleading_template. case_heading, case_no: @pleading_template.case_no, case_title: @pleading_template.case_title, copy_furnished: @pleading_template.copy_furnished, nature_of_case: @pleading_template.nature_of_case, pleadings_intro: @pleading_template.pleadings_intro }
    end

    assert_redirected_to pleading_template_path(assigns(:pleading_template))
  end

  test "should show pleading_template" do
    get :show, id: @pleading_template
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pleading_template
    assert_response :success
  end

  test "should update pleading_template" do
    patch :update, id: @pleading_template, pleading_template: {  case_heading: @pleading_template. case_heading, case_no: @pleading_template.case_no, case_title: @pleading_template.case_title, copy_furnished: @pleading_template.copy_furnished, nature_of_case: @pleading_template.nature_of_case, pleadings_intro: @pleading_template.pleadings_intro }
    assert_redirected_to pleading_template_path(assigns(:pleading_template))
  end

  test "should destroy pleading_template" do
    assert_difference('PleadingTemplate.count', -1) do
      delete :destroy, id: @pleading_template
    end

    assert_redirected_to pleading_templates_path
  end
end
