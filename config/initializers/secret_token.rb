# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
PleadingApp::Application.config.secret_key_base = 'd1f6ff786c4c2955db6fd18a7977255cb4b31590067f36b7528b0ffac3304eb1860f7f53bf6e9ac5b9992ec5f1a82e32baa2c430d218df1fbc97409bd0a8ff6a'
