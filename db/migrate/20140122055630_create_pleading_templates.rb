class CreatePleadingTemplates < ActiveRecord::Migration
  def change
    create_table :pleading_templates do |t|
      t.text :case_heading
      t.text :case_title
      t.text :case_no
      t.text :nature_of_case
      t.text :pleadings_intro
      t.text :copy_furnished

      t.timestamps
    end
  end
end
